# Summary

* [Introduction](README.md)

* [Competitions](docs/competitions.md)

* [Mercari Price Suggestion Challenge](docs/Mercari_Price_Suggestion_Challenge.md)
  - [Konstantin](docs/Mercari_Price_Suggestion_Challenge/Konstantin.md)
